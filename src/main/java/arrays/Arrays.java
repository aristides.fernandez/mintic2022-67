
package arrays;


public class Arrays {
    
    
    public static void main(String[] args) {
        
        int[] array = new int[6]; // forma de crear un arreglo de tipo numero
        // en el momento que lo creo todas las posiciones del arreglo toman el valor de cero
        array[0] = 1; // así se asignar el valor a un elemento de ese arreglo 
        // solo se deja asignar siempre y cuando acceda a un indice valido
        
        int suma = array[0] +2; // realizar operaciones en base a un valor de ese arreglo
       
        System.out.println("--- suma "+suma);
        
        
        //-------------------- cuidado con la excepciones //
        
        suma = array[5] +2;  // excepción en tiempo de ejecución
        
        System.out.println("--- suma "+suma);
        
        
        // -------------------- otra forma de inicializar y asignar valores a un arreglo
        
        
        int[] array2 = new int[] {1, 2, 3, 4, 5 };  // int[] array2 = {1, 2, 3, 4, 5 };
        
        System.out.println("--- array2[0] "+array2[0]);
         
        // -------------------- recorrer un arreglo mediante un for
         
        for (int i = 0; i < array2.length; i++) {
            System.out.println(array2[i]);
        }
        
        // Que se pueden hacer o valdiar con los vectore //
        
        
        
          int[] array3 = new int[] {1, 2, 3, 4, 5 }; 
          int[] array4 = new int[] {1, 2, 3, 4, 5 }; 

          if(array3 == array4){
             System.out.println("son iguales ");                 
          }else{
             System.out.println("No son iguales "); // no son iguales a pesar de tener el mismo contenido
          }
          
                    
          
          //------------------------------------
            int[] array5 = array4; // cuanto espacios de memoria acabo de separar 1

            if(array5 == array4){
               System.out.println("son iguales ");   // apunta a la misma referencia               
            }else{
               System.out.println("No son iguales ");
            }
            
                        
            
            //------------------------------------ 
            array4[0] = 22;
            if(array5[0] == array4[0]){
               System.out.println("son iguales ");   // apunta a la misma referencia               
            }else{
               System.out.println("No son iguales ");
            }
            
            //------------------------------------ 
            array5 = new int[3];
            
            if(array5[0] == array4[0]){
               System.out.println("son iguales ");                
            }else{
               System.out.println("No son iguales ");// no son iguales ya cambia la referencia
            }
            
            
            
            //------------------------------------ MATRICES ----------------------//
            
            // cuando se crea la matriz con el new se crea un vector de referencia, donde
            // cada referencia apunta a un vector de direcciones de memeoria donde se almacena los datos
            // del tipo que fue declarada
            
            int[][] matriz = new int[2][2];
            int[][] matriz2 = new int[][]{ {1,2}, {3,4} };
        
            int[][] matriz3 = new int[2][];  // el numero de columnas apuntan a un espacio indeterminado conocido como null
            //int[][] matriz4 = new int[][3]; // error no se puede          

            matriz3[0][1] = 2; 
            
            System.out.println("son iguales "+matriz3[0][1]);
            
            
            //------------------------------
            
                    
          int[][] matriz4 = new int[2][];           
    
          matriz4[0] = new int[2];
          matriz4[1] = new int[3];
          
          matriz4[0][1] = 2;   
            
          matriz4[1][2] = 32; 
          
           
           System.out.println("matriz4 "+ matriz4.length); // me devuelve el numero de filas de la matriz
            
           
        //------------------------------   
           
        /*
                     0  1    
            0 -->    1, 2
            1 -->    3, 4
        */

        
          int[][] matriz5 = new int[][]{ {1,2}, {3,4} };        
        
          System.out.println("matriz4 "+ matriz5[1][1]); // me devuelve el numero de filas de la matriz
          
          
          
         //------------------------------ ASIGNAR UN VECTOR EN UNA MATRIZ
          
          int[][] matriz6 = new int[2][3];
          
          int[] vect = new int[]{1,2,3,4};
          
          matriz6[1] = vect;    // es una referencia que contiene la dirección del primer array de enteros  
  
           
           System.out.println("matriz6 "+ matriz6[1][3]); 
           
           
          //------------------------------ RECORRER UNA MATRIZ  
         int[][] matriz7 = new int[][]{ {1,2,3 }, {4,5,6} , {7,8, 9}};
          
        
          
          for(int i = 0; i < matriz6.length; i++){
              
                for(int j = 0; j < matriz7[0].length; j++){
                    System.out.print(matriz7[i][j]);
                }
               System.out.println();
          }
      
            
            
            
        
    }
    
}
