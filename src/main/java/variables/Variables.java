package variables;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author aristides
 */
public class Variables {
    
    public static void main(String[] args) {
          // variables // tipos de datos
        
        // tipo numerico //
        // byte --> 8 bits  --> 2 ^ (8-1) = -128 ~ 127
        // short --> 16 bits --> 2 ^ (16-1)
        // int --> 32 bits   --> 2 ^ (32-1)
        // long --> 64 bits --> 2 ^ (64-1)
        
        float num = 2331.5555555554F;
        num = 23.1f;
        
        double num2 = 2331.5555555554;
        
        boolean hoyEsMiercoles = true;
        
        char caracter =  'f';
        
        System.out.println("float "+ num);
        System.out.println("double "+ num2);
        
        String cadena = "fsclsl";
        
        cadena = cadena.toUpperCase();
        
        System.out.println("cadena: " + cadena);
    }
    
}
