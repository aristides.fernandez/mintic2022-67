/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package poo;

/**
 *
 * @author aristides
 */
public class Fecha {
    
    
    private static final String[] DIAS = {"L", "M", "M", "J","V","S","D"};
    
    
  public int getIndice(String letra){
       for (int i = 0; i < DIAS.length; i++){
                if (DIAS[i].equals(letra)){
                    return i;
                }
        }                          
        return -1;
        
     } 
    
 
    
}
