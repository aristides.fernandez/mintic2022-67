
package poo;

public interface VolumenGraduable {
       
    void subirVolumen();
    void bajarVolumen();
    
}
