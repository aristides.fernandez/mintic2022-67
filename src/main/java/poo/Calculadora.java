/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package poo;

/**
 *
 * @author aristides
 */
public class Calculadora {
    
    public static final double PI = 3.14163333;  // 
    
    private double valor;
    
    public Calculadora(double valor){
        this.valor = valor;
    }
    
    
    public static double sumar(double a, double b){
        return PI + a + b ;
    }
    
    public void hacer(){
        System.out.println("--- " + PI * valor);
    }
    
}
