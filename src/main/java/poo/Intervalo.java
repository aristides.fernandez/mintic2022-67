
package poo;


public class Intervalo {
    
    private double inferior;    
    private double superior; 
        
    public Intervalo(){ 
    }
    
    public Intervalo(double inferior, double superior){
        this.inferior = inferior;
        this.superior = superior;
    }  

    public Intervalo(double superior){ 
        this.superior = superior;
    } 
    
    public double longitud(){         
        return superior - inferior;
    }
    
    public double puntoMedio(){
        return inferior + longitud()/2;
    }
       
    public void oponer(){        
        double inferiorInicial = inferior; // 20
        inferior = superior;  // 50
        superior = inferiorInicial; // 20
    }
    
    public void mostrar(){        
        System.out.println("inferior " + inferior);
        System.out.println("superior " + superior);        
    }
      
    public void desplazar(double value){
        inferior += value;
        superior += value;
    }
    
    public boolean incluir(double value){
        return this.inferior <= value && value <= this.superior;
    }
        
    public boolean incluir(Intervalo intervalo){ 
              // (20     <=    25  ) true         &&      (  50      <=    50 ) true
        //return this.inferior <= intervalo.inferior && intervalo.superior <= this.superior;        
                          
        return incluir(intervalo.inferior) && incluir(intervalo.superior);
    }
    
    public boolean igual(Intervalo intervalo){
        return this.inferior == intervalo.inferior && this.superior == intervalo.superior; 
    }
        
    public void doblar(){
        
        double longitudInicial = longitud();
        inferior -= longitudInicial/2;
        superior += longitudInicial/2;
    }
    
    
}
