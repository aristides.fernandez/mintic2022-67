
package poo;

public abstract class SerVivo {     
    
    private static final int VALOR = 5;
    
    
    public abstract void alimentarse();
    
    public void crecer(){
        System.out.println("creciendo ... " + VALOR);
    }
    
}
