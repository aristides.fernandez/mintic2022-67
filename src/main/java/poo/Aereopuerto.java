
package poo;


public class Aereopuerto {
    
    private String nombre;    
    private int[] pistas;
    private int capacidadAviones;
    private int numeroSalas;
    private int[] pistaOcupadas;
    private Aviones[] aviones;
    
    int valor = 5;    
    Integer data = valor;  
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int[] getPistas() {
        return pistas;
    }

    public void setPistas(int[] pistas) {
        this.pistas = pistas;
    }

    public int getCapacidadAviones() {
        return capacidadAviones;
    }

    public void setCapacidadAviones(int capacidadAviones) {
        this.capacidadAviones = capacidadAviones;
    }

    public int getNumeroSalas() {
        return numeroSalas;
    }

    public void setNumeroSalas(int numeroSalas) {
        this.numeroSalas = numeroSalas;
    }

    public int[] getPistaOcupadas() {
        return pistaOcupadas;
    }

    public void setPistaOcupadas(int[] pistaOcupadas) {
        this.pistaOcupadas = pistaOcupadas;
    }   

    
    public Aereopuerto(String nombre, int[] pistas, int capacidadAviones, int numeroSalas){
        this.nombre = nombre;
        this.pistas = pistas;
        this.capacidadAviones = capacidadAviones;
        this.numeroSalas = numeroSalas;   
        
        this.pistaOcupadas = new int[pistas.length];
        
        for(int i = 0; i < pistaOcupadas.length; i++){
            pistaOcupadas[i] = -1;  // {-1, -1, -1} antes  -- despues {8, -1, -1}
        }       

    }   
    
      public Aereopuerto(String nombre, int capacidadAviones, int numeroSalas){
        this.nombre = nombre;   
        this.capacidadAviones = capacidadAviones;
        this.numeroSalas = numeroSalas;         
    }   
    
    
    public void mostrarPistaOcupadas(){
        for(int i = 0; i < pistaOcupadas.length; i++){
            System.out.println(" pistaOcupadas[ " + i +" ] " + pistaOcupadas[i] ); 
                // {-1, -1, -1} antes  -- despues {8, -1, -1}
        }         
    }
    
    public boolean hayPistasDisponibles(){        
        for(int i = 0; i < pistaOcupadas.length; i++){
            
            if(pistaOcupadas[i] == -1){
                return true;
            }
        }        
        return false;
    }
    
    public void ocuparPista(int pista){
        
        for(int i = 0; i < pistaOcupadas.length; i++){
            
            if(pistaOcupadas[i] == -1){
                pistaOcupadas[i] = pista;
                break;
            }
        }  
    } 
      
     public void liberarPista(int pista){  
         
         System.out.println(" liberar pista # " + pista ); 
         
        for (int i = 0; i < pistaOcupadas.length; i++) {
            if (pistaOcupadas[i] == pista) {                     
                pistaOcupadas[i] = -1;               
                break;
            }
        }
    }
    
    public boolean pistaLibre(int pista){
        
        for (int i = 0; i < pistaOcupadas.length; i++) {
            if (pistaOcupadas[i] == pista) {                    
                return false;
            }
        }
        return true;
    }
    
}
