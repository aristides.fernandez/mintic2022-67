
package poo;

public class IntervaloNew {
    
    private double longitud;    
    private double puntoMedio; 
    
    private double getSuperior() {
        return puntoMedio + longitud/2;
    }

    private void setSuperior(double superior) {
        this.longitud = superior - getInferior();
        this.puntoMedio = superior - longitud/2;
    }

  
    private double getInferior() {
        return puntoMedio - longitud/2;
    }

    private void setInferior(double inferior) {
        this.longitud = getSuperior() - inferior;
        this.puntoMedio = inferior + longitud/2;
    }
        
       
    public IntervaloNew(){ 
    }
    
    public IntervaloNew(double inferior, double superior){
        this.setInferior(inferior);
        this.setSuperior(superior);
    }  

    public IntervaloNew(double superior){ 
        this.setSuperior(superior);
    } 
    
    public double longitud(){         
        return getSuperior() - getInferior();
    }
    
    public double puntoMedio(){
        return getInferior() + longitud()/2;
    }
    
    public void oponer(){        
        double inferiorInicial = getInferior(); // 20
        setInferior(getSuperior());  // 50
        setSuperior(inferiorInicial); // 20
    }
    
    public void mostrar(){        
        System.out.println("inferior " + getInferior());
        System.out.println("superior " + getSuperior());        
    }
      
    public void desplazar(double value){
        setInferior(getInferior() + value);
        setSuperior(getSuperior() + value);
    }
    
    public boolean incluir(double value){
        return this.getInferior() <= value && value <= this.getSuperior();
    }
        
    public boolean incluir(IntervaloNew intervalo){ 
              // (20     <=    25  ) true         &&      (  50      <=    50 ) true
        //return this.inferior <= intervalo.inferior && intervalo.superior <= this.superior;        
                          
        return incluir(intervalo.getInferior()) && incluir(intervalo.getSuperior());
    }
    
    public boolean igual(IntervaloNew intervalo){
        return this.getInferior() == intervalo.getInferior() && this.getSuperior() == intervalo.getSuperior(); 
    }
        
    public void doblar(){
        
        double longitudInicial = longitud();
        setInferior(getInferior() - longitudInicial/2);
        setSuperior(getSuperior() + longitudInicial/2);
    }
    
    
}
