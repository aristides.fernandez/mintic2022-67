
package cine;

public class TarjetaCine {
    //Atributos
    private String idTarjeta;
    private String nombreCompleto;
    private String email;
    private String telefono;
    private int edad;
    private double porcentajeDescuento;
    

    //Constructor
    public TarjetaCine(String idTarjeta, String nombreCompleto, String email, String telefono, int edad, double porcentajeDescuento) {
        this.idTarjeta = idTarjeta;
        this.nombreCompleto = nombreCompleto;
        this.email = email;
        this.telefono = telefono;
        this.edad = edad;
        this.porcentajeDescuento = porcentajeDescuento;
    }

    //Método
    public double pagar(String[] cuenta){
        return obtenerTotal(cuenta) * (1 - porcentajeDescuento / 100);
    }

    //Boleta = 6000
    //Combo 1 - Crispetas + Gaseosa = 8000
    //Combo 2 - Perro + Gaseosa = 12000   

    private double obtenerTotal(String[] cuenta) {

        double total = 0;

        for(int i = 0; i < cuenta.length; i++){

            if("Boleta".equals(cuenta[i])){
                total += 6000; 
            }else if("Combo 1 - Crispetas + Gaseosa".equals(cuenta[i])){
                total += 8000; 
            }else if("Combo 2 - Perro + Gaseosa".equals(cuenta[i])){
                total += 12000; 
            }
        }
        return total;
    }

    //Getters y Setters
    public String getIdTarjeta() {
        return "";
    }

    public void setIdTarjeta(String idTarjeta) {
        
    }

    public String getNombreCompleto() {
         return "";
    }

    public void setNombreCompleto(String nombreCompleto) {
        
    }

    public String getEmail() {
         return "";
    }

    public void setEmail(String email) {
        
    }

    public double getPorcentajeDescuento() {
        return 0;
    }

    public void setPorcentajeDescuento(double porcentajeDescuento) {
        
    }

    public String getTelefono() {
         return "";
    }

    public void setTelefono(String telefono) {
        
    }

    public int getEdad() {
        return 0;
    }

    public void setEdad(int edad) {
        
    }

    public String getDescripcion() {
        return "idTarjeta= " + idTarjeta + " nombreCompleto= "+ nombreCompleto + " email= "+ email+
            " telefono= "+ telefono + " edad= "+ edad +" porcentajeDescuento= "+ porcentajeDescuento;
    }
}