/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cine;

/**
 *
 * @author aristides
 */
public class TarjetaOro extends TarjetaCine {
    //Atributos
    

    //Constructor
    public TarjetaOro(String idTarjeta, String nombreCompleto, String email, String telefono, int edad) {
        super(idTarjeta, nombreCompleto, email, telefono, edad, 20);
    }

    //Método
    public boolean redimirBeneficio(int posicionBeneficio){
        return false;
    }

    //Getters y Setters
    public String[] getBeneficiosDescripcion() {
        return new String[]{};
    }

    public void setBeneficiosDescripcion(String[] beneficiosDescripcion) {
        
    }

    public boolean[] getBeneficiosEstado() {
        return new boolean[]{};
    }

    public void setBeneficiosEstado(boolean[] beneficiosEstado) {
        
    }
}
