package com.mycompany.mintic2022;

import java.io.*;
import javax.swing.JOptionPane;


public class Mintic2022 {   
       
    public static void main(String[] args) {
        //flujoEscribir();

        File archivo;
        File archivoBackup;
        FileReader leer;
        BufferedReader almacenar;
        FileReader leerBackup;
        BufferedReader almacenarBackup;

        archivo = new File("invetario.txt");
        archivoBackup = new File("invetarioBackup.txt");

        System.out.println(" existe archivo " + archivo.exists());
        System.out.println(" existe archivoBackup " + archivoBackup.exists());

        try {

            leer = new FileReader(archivo);
            almacenar = new BufferedReader(leer);

            leerBackup = new FileReader(archivoBackup);
            almacenarBackup = new BufferedReader(leerBackup);
            
            String data = "";
            String dataBackup = "";
            boolean isEquals = true;

            while(data != null){

                try {

                    data = almacenar.readLine();
                    dataBackup = almacenarBackup.readLine();
                                    
                    if(data != null && dataBackup !=null){
                        
                        if(!data.equals(dataBackup)){
                            isEquals = false;
                            data = null;
                        } 
                    }

                } catch (IOException | NullPointerException e) {
                    JOptionPane.showMessageDialog(null, "Error al leer el archivo "+ e);
                }

            }

            if(isEquals){                
                JOptionPane.showMessageDialog(null, "Los archivos son iguales");
            }else{
                JOptionPane.showMessageDialog(null, "Los archivos son diferentes");
            }

        } catch (FileNotFoundException e) {
            JOptionPane.showInputDialog(null, "Archivo no encontrado");
        }
    }

    static void flujoEscribir(){

        File archivo = new File("invetario.txt");

        if(!archivo.exists()){
            try {
                archivo.createNewFile(); 
                escribir(archivo);           
     
            } catch (IOException ex) {
                JOptionPane.showInputDialog(null, "ha ocurrido un error");
            }
        }else{
            escribir(archivo);  
        }     
    }

    static void escribir(File archivo){

        try {

            FileWriter escritura = new FileWriter(archivo, true);
            PrintWriter lineaGuia = new PrintWriter(escritura);

            String codigo = JOptionPane.showInputDialog(null, "ingrese el codigo");
            String producto  = JOptionPane.showInputDialog(null, "ingrese el producto");  
            
            lineaGuia.println(codigo);
            lineaGuia.println(producto);
         
            escritura.close();
            lineaGuia.close();

        } catch (IOException ex) {
            JOptionPane.showInputDialog(null, "ha ocurrido un error");
        }
    }

}
