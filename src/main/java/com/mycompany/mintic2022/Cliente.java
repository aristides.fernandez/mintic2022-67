/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.mintic2022;

/**
 *
 * @author aristides
 */
public class Cliente {
    
   private String nombreCompleto;
   private String documentoIdentidad;
   private int totalAPagar;
   private String fechaCompra;
   private String numeroFactura;
   
   public Cliente(String nombreCompleto,String documentoIdentidad,int totalAPagar,String fechaCompra,String numeroFactura){
   this.nombreCompleto = nombreCompleto;
   this.documentoIdentidad = documentoIdentidad;
   this.totalAPagar = totalAPagar;
   this.fechaCompra =fechaCompra;
   this.numeroFactura =numeroFactura;
   }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    @Override
    public String toString() {
        return "cliente{" + "nombreCompleto=" + nombreCompleto + ", documentoIdentidad=" + documentoIdentidad + ", totalAPagar=" + totalAPagar + ", fechaCompra=" + fechaCompra + ", numeroFactura=" + numeroFactura + '}';
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public String getDocumentoIdentidad() {
        return documentoIdentidad;
    }

    public void setDocumentoIdentidad(String documentoIdentidad) {
        this.documentoIdentidad = documentoIdentidad;
    }

    public int getTotalAPagar() {
        return totalAPagar;
    }

    public void setTotalAPagar(int totalAPagar) {
        this.totalAPagar = totalAPagar;
    }

    public String getFechaCompra() {
        return fechaCompra;
    }

    public void setFechaCompra(String fechaCompra) {
        this.fechaCompra = fechaCompra;
    }

    public String getNumeroFactura() {
        return numeroFactura;
    }

    public void setNumeroFactura(String numeroFactura) {
        this.numeroFactura = numeroFactura;
    }
    
}
