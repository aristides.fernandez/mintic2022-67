
package com.mycompany.mintic2022;


public class Calculadora {

    public Double cuadrado(Double numero) {
        return numero * numero;
    }

    public Double cubo(Double numero) {
        return numero * numero * numero;
    }
} 