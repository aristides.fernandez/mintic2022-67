/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.mintic2022;

import java.util.ArrayList;

/**
 *
 * @author aristides
 */
public class Reporte {
    
    
    public static Object[] getReporte(ArrayList <Cliente> tienda){
        
        int promedio = 0;
        int suma = 0;
        
        for (int i=0;i<tienda.size();i++){
          suma = suma + tienda.get(i).getTotalAPagar();
        }
        
        String nombremenor = tienda.get(0).getNombreCompleto();        
        String nombre = tienda.get(0).getNombreCompleto();
        
        promedio = suma/tienda.size();
        int  m = tienda.get(0).getTotalAPagar();
        int Nmayor = tienda.get(0).getTotalAPagar();
        
        for (int i=1; i<tienda.size(); i++){
            
            if (m > tienda.get(i).getTotalAPagar()){
                m = tienda.get(i).getTotalAPagar();
                nombremenor = tienda.get(i).getNombreCompleto();
            }
            if (Nmayor<tienda.get(i).getTotalAPagar()){
                Nmayor = tienda.get(i).getTotalAPagar();
                nombre = tienda.get(i).getNombreCompleto();
            }
        }
        
       
        return new Object[]{promedio,nombremenor,m,nombre,Nmayor}; 
    }
    
}
